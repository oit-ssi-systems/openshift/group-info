#!/usr/bin/env python3

'''
Looks for PVCs and quota objects to report back to
exchequer
'''

from kubernetes import client, config
from openshift.dynamic import DynamicClient
import os
import logging
import openshift.dynamic.exceptions
from flask import Flask, jsonify

app = Flask(__name__)
logging.getLogger(__name__)
config.load_incluster_config()
oc_client = DynamicClient(client.ApiClient())

project_skip_list = os.environ.get("PROJECT_SKIP_LIST", None).split(',')

def fetch_fundcode(project):
    '''
    Returns a string fundcode.
    '''
    try:
        # get a list of fundcodes
        v1_fundcodes = oc_client.resources.get(api_version='duke.edu/v1', kind='FundCode')
        # get our fundcode for this project, e.g. CG-OKD
        if len(v1_fundcodes.get(namespace=project.metadata.name).items) == 1:
            fundcode = v1_fundcodes.get(namespace=project.metadata.name).items[0].spec.code
            return fundcode
    except openshift.dynamic.exceptions.ResourceNotFoundError:
        return ''


def fetch_quota(project):
    '''
    Returns a hash of memory limits set in the resource quota
    '''
    # get a list of quota objects
    v1_resourcequotas = oc_client.resources.get(api_version='v1', kind='ResourceQuota')
    try:
        # loop over quotas to get set limits
        for quota in v1_resourcequotas.get(namespace=project.metadata.name).items:
            memreq_gi = quota.spec.hard["requests.memory"]
            r_type = type(memreq_gi)
            memreq = float(r_type().join(filter(r_type.isdigit, memreq_gi)))
            return {
                        'description': 'RAM Request Limit: ' + memreq_gi,
                        'service_type': 'OKD RAM Requests',
                        'amount': str(round(memreq * project_memory_rate / 12, 2))
                    }
    except openshift.dynamic.exceptions.ResourceNotFoundError:
        return {}


def fetch_pvc(project):
    '''
    Fetches PVC claim size data for the project
    '''
    v1_pvclaims = oc_client.resources.get(api_version='v1', kind='PersistentVolumeClaim')
    all_claims = []
    try:
        # loop over pvcs to get requested size
        for pvc in v1_pvclaims.get(namespace=project.metadata.name).items:
            storage_requested_gi = pvc.status.capacity.storage
            r_type = type(storage_requested_gi)
            storage_requested = float(r_type().join(filter(r_type.isdigit, storage_requested_gi)))
            all_claims.append({
                    'description': 'Persistent Volume Claim: ' + pvc.metadata.name + ' Size: ' + storage_requested_gi,
                    'service_type': 'OKD PVC Size',
                    'amount': str(round(storage_requested * project_storage_rate / 12, 2))
                })
    except openshift.dynamic.exceptions.ResourceNotFoundError:
        # we didn't have any
        all_claims.append({})
    return all_claims

def fetch_admin_group(project):
    '''
    Fetches the group with admin role from the project
    '''
    rolebindings = []
    v1_rolebindings = oc_client.resource.get(api_version='v1', kind='RoleBindings')
    try:
        for rolebinding in v1_rolebindings.get(namespace=project.metadata.name).items:
            rolebindings.append(rolebinding)
    except openshift.dynamic.exceptions.ResourceNotFoundError:
        rolebindings.append({})
    return rolebindings


@app.route('/')
def generate_report():
    '''
    Generates and returns a jsonified object corresponding to spec for exchquer
    '''
    group_list = []
    v1_projects = oc_client.resources.get(api_version='project.openshift.io/v1', kind='Project')

    project_list = v1_projects.get()

    for project in project_list.items:
        if project.metadata.name in project_skip_list:
            continue
        project_group_data = {
            'name': project.metadata.name,
            'admin-group': '',
            'policy-group': ''
            }
        project_group_data.update({'admin-group': fetch_admin_group(project)})
        project_group_data.update({'policy-group': 'oit_okd_' + project_group_data['admin-group']})
        group_list.append(project_group_data)
    return jsonify(group_list)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
