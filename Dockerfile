FROM ops-python-openshift

LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Office of Information Technology' \
      architecture='x86_64' \
      summary='group-info reports on group ownership' \
      description='Creates a report on project admininstrative ownership' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/oit-ssi-systems/openshift/group-info'

LABEL version='1.0' \
      release='1'

ENV DEBIAN_FRONTEND=noninteractive \
    PYTHONUNBUFFERED=0

ADD app /app
WORKDIR /app

RUN pip3 install -r requirements.txt

EXPOSE 5000

CMD ["python3", "app.py"]
